from pathlib import Path

import pytest


@pytest.fixture
def day_one_part_one_input() -> Path:
    file_path = "aoc_2020/tests/fixtures/input.txt"
    return Path(file_path)


@pytest.fixture
def day_two_input() -> Path:
    file_path = "aoc_2020/tests/fixtures/input_day_two.txt"
    return Path(file_path)


@pytest.fixture
def day_three_input() -> Path:
    file_path = "aoc_2020/tests/fixtures/input_day_three.txt"
    return Path(file_path)


@pytest.fixture
def day_four_input() -> Path:
    file_path = "aoc_2020/tests/fixtures/input_day_four.txt"
    return Path(file_path)
