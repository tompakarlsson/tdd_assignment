from aoc_2020.src.day_three.day_three_part_one import solve_part_one
from aoc_2020.src.day_three.day_three_part_two import solve_part_two


def test_day_three_part_one(day_three_input):
    # GIVEN a fixture with test data from AOC day three
    filepath = day_three_input

    # WHEN calling solution with our set of data
    solution = solve_part_one(indata=filepath)

    # THEN assert that solution returns the correct answer: 2
    assert solution == 7


def test_day_three_part_two(day_three_input):
    # GIVEN a fixture with test data from AOC day three
    filepath = day_three_input

    # WHEN calling solution with our set of data
    solution = solve_part_two(indata=filepath)

    # THEN assert that solution returns the correct answer: 336
    assert solution == 336
