from aoc_2020.src.day_four.day_four_part_one import solve_part_one


def test_day_four_part_one(day_four_input):
    # GIVEN a fixture with test data from AOC day four
    filepath = day_four_input

    # WHEN calling solution with our set of data
    solution = solve_part_one(indata=filepath)

    # THEN assert that solution returns the correct answer: 2
    assert solution == 2
