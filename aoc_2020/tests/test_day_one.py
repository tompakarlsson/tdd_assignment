from aoc_2020.src.day_one.day_one_part_one import solve
from aoc_2020.src.day_one.day_one_part_two import solve_part_two


def test_day_one_part_one(day_one_part_one_input):
    # GIVEN a fixture with test data from AOC day one part one
    filepath = day_one_part_one_input

    # WHEN calling solution with our set of data
    solution = solve(indata=filepath)

    # THEN assert that solution returns the correct answer: 514579 (1721 * 299)
    assert solution == 514579


def test_day_one_part_two(day_one_part_one_input):
    # GIVEN a fixture with test data from AOC day one part one
    filepath = day_one_part_one_input

    # WHEN calling solution with our set of data
    solution = solve_part_two(indata=filepath)

    # THEN assert that solution returns the correct answer: 514579 (1721 * 299)
    assert solution == 241861950
