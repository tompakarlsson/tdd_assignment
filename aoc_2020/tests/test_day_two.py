from aoc_2020.src.day_two.day_two_part_one import solve_part_one
from aoc_2020.src.day_two.day_two_part_two import solve_part_two


def test_day_two_part_one(day_two_input):
    # GIVEN a fixture with test data from AOC day two
    filepath = day_two_input

    # WHEN calling solution with our set of data
    solution = solve_part_one(indata=filepath)

    # THEN assert that solution returns the correct answer: 2
    assert solution == 2


def test_day_two_part_two(day_two_input):
    # GIVEN a fixture with test data from AOC day two
    filepath = day_two_input

    # WHEN calling solution with our set of data
    solution = solve_part_two(indata=filepath)

    # THEN assert that solution returns the correct answer: 1
    assert solution == 1
