"""advent of code 2020 day three part one"""

from pathlib import Path


def solve_part_one(indata: Path) -> int:
    with open(Path(indata), 'r') as file:
        input_data: list[str] = list(file.read().strip('\n').splitlines())
    x = 0
    y = 0
    tree_hit = 0
    while y < len(input_data)-1:
        y += 1
        x = (x + 3) % len(input_data[0])
        if input_data[y][x] == '#':
            tree_hit += 1
    return tree_hit


if __name__ == '__main__':  # pragma: no cover
    print(solve_part_one(Path('../../input_data/input_day_three.txt')))

