"""advent of code 2020 day three part two"""

from pathlib import Path


def solve_part_two(indata: Path) -> int:
    with open(Path(indata), 'r') as file:
        input_data: list[str] = list(file.read().strip('\n').splitlines())

    slopes = [{'x': 1, 'y': 1},
              {'x': 3, 'y': 1},
              {'x': 5, 'y': 1},
              {'x': 7, 'y': 1},
              {'x': 1, 'y': 2}]

    total_amount_hits = 1

    for slope in slopes:
        x = 0
        y = 0
        tree_hit = 0
        while y < len(input_data) - 1:
            y += slope['y']
            x = (x + slope['x']) % len(input_data[0])
            if input_data[y][x] == '#':
                tree_hit += 1
        total_amount_hits *= tree_hit

    return total_amount_hits


if __name__ == '__main__':  # pragma: no cover
    print(solve_part_two(Path('../../input_data/input_day_three.txt')))
