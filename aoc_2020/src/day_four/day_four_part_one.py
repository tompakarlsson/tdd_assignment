"""advent of code 2020 day four part one"""

from pathlib import Path

fields = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}


def solve_part_one(indata: Path) -> int:
    count_valid_passports = 0
    with open(Path(indata), 'r') as file:
        input_data: list[str] = file.read().split('\n\n')
        for passport in input_data:
            passport = passport.replace('\n', ' ')
            count_fields = sum(field in passport for field in fields)
            if count_fields == 7:
                count_valid_passports += 1
    return count_valid_passports


if __name__ == '__main__':  # pragma: no cover
    print(solve_part_one(Path('../../input_data/input_day_four.txt')))
