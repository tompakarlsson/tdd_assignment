"""advent of code 2020 day two part one"""

from pathlib import Path


def solve_part_two(indata: Path) -> int:
    res: int = 0
    with open(Path(indata), 'r') as file:
        input_data: list[str] = list(file.read().strip('\n').splitlines())
        for piece in input_data:
            piece = piece.split(' ')
            piece[0] = piece[0].split('-')
            character = piece[1].strip(':')
            password = piece[2]
            low = int(piece[0][0])
            high = int(piece[0][1])

            if password[low - 1] == character and password[high - 1] != character or password[low - 1] != character and password[high - 1] == character:
                res += 1
    return res


if __name__ == '__main__':  # pragma: no cover
    print(solve_part_two(Path('../../input_data/input_day_two.txt')))
