"""advent of code 2020 day one part one"""
from pathlib import Path


def solve(indata: Path) -> int:
    with open(Path(indata), 'r') as file:
        input_data: set[int] = {int(x) for x in file.read().strip('\n').splitlines()}
    for first_int in input_data:
        for second_int in input_data:
            if first_int + second_int == 2020:
                return first_int * second_int


if __name__ == '__main__':  # pragma: no cover
    print(solve(Path('../../input_data/input.txt')))
